---
layout: page
title: Università
permalink: /universita/
---


## Tesi triennale

La mia tesi triennale, svolta sotto la supervisione del prof. [Andrea Maffei](https://people.dm.unipi.it/maffei/), si colloca nell'ambito della rappresentazioni dei gruppi algebrici lineari e ha lo scopo di dimostrare la decomposizione di Bruhat ed il teorema di Borel-Weil.
Il lavoro svolto è pubblicamente accessibile:
- [repository](https://www.gitlab.com/Ventu06/tesi-triennale/) contenente il sorgente della tesi e delle slides;
- [tesi](https://ventu06.gitlab.io/tesi-triennale/tesi.pdf) in formato pdf;
- [slides](https://ventu06.gitlab.io/tesi-triennale/slides-tesi.pdf) della presentazione in formato pdf.


## Pagine utili

Ecco una lista di siti e pagine personali contenenti appunti ed altri documenti utili:
{% for page in site.data.data.universita.pages %}
- [{{ page.name }}]({{ page.link }}) \
  Contenuto:{% for course in page.content %} {{ course.name }}{% if course.prof %} ({{ course.prof }}){% endif %};{% endfor %}
{% endfor %}
