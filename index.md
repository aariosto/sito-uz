---
layout: page
title: Home
permalink: /
---

Sono uno studente di Matematica iscritto al corso ordinario presso la [Scuola Normale Superiore](https://www.sns.it/) e al corso di laurea magistrale presso l'[Università di Pisa](https://www.dm.unipi.it/).

Collaboro come amministratore di sistema con la SNS per la gestione di Starfleet e di [UZ](https://uz.sns.it), cioè l'infrastruttura informatica del collegio Timpano e la rete informatica degli allievi.


## Curriculum Vitae

Scaricabile [qui](https://ventu06.gitlab.io/curriculum-vitae/cv.pdf).


## Contatti

{% for contact in site.data.data.contacts %}
<i class="{{ contact.icon }}"></i> [{{ contact.name }}]({{ contact.link }})
{% endfor %}
